﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ScrapingWinForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            try
            {
                if (btn_Login.Cursor == Cursors.Hand)
                {
                    th = new Thread(Result);
                    th.Start();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        private ChromeDriver drv;
        private Thread th;



      

        private void Result()
        {
            try
            {

                btn_Login.ForeColor = Color.Gold;
                btn_Login.UseWaitCursor = true;
                btn_Login.Text = "Testingggg...";
                OpenSelenium();
                Thread.Sleep(5000);
                Login(txt_UserName.Text, txt_Password.Text);
                if (TestAccount())
                {
                    MessageBox.Show("Kullanıcı Adı veya Şifre Hatalı", "Uyarı", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);

                }
                else
                {
                    MessageBox.Show("Giriş Başarılı Bir Şekilde Tamamlandı.", "Bilgi", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                CloseSelenium();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void Login(string userName, string password)
        {
            try
            {

                drv.FindElements(By.XPath("//input[@class='AnimatedForm__textInput ']"))[0].SendKeys(userName);
                Thread.Sleep(3000);
                drv.FindElements(By.XPath("//input[@class='AnimatedForm__textInput ']"))[1].SendKeys(password);
                Thread.Sleep(3000);
                drv.FindElement(By.XPath("//button[@class='AnimatedForm__submitButton m-full-width']")).Click();
                Thread.Sleep(3000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }

        }

        private void OpenSelenium()
        {
            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;//hide Console
            drv = new ChromeDriver(service);
            drv.Navigate().GoToUrl("https://www.reddit.com/login/?dest=https%3A%2F%2Fwww.reddit.com%2F");
        }

        private void CloseSelenium()
        {
            drv.Quit();
        }

        

        private bool TestAccount()
        {
            try
            {
                if (drv.Url == "https://www.reddit.com/login/?dest=https%3A%2F%2Fwww.reddit.com%2F")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        private void txt_UserName_TextChanged(object sender, EventArgs e)
        {
            if (txt_Password.Text != "" && txt_UserName.Text != "")
            {
                btn_Login.ForeColor = Color.Chartreuse;
                btn_Login.Cursor = Cursors.Hand;
            }
            else
            {
                btn_Login.ForeColor = Color.Red;
                btn_Login.Cursor = Cursors.No;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseSelenium();
        }
    }
}
